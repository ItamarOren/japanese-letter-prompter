///@description
ini_open(file);

length = ini_read_real(sect, "Total", 0);

#region ALL
if (type == "all")
{
	for (var i = -1; i < length; i ++)
	{
		TEXT = ini_read_string(sect, i, "none");
		if (TEXT != undefined && TEXT != "none")
		{
			if (global.type == "drawn")
			{
				MultiTextGenerator();
				break;
			}
			else show_message(TEXT);
		}
	}
}
#endregion

#region RANDOM ALL
else if (type = "random all")
{
	//arr[0] = "moot";
	var _num, map;
	map = ds_map_create();
	
	for (var i = 0; i <= length; i ++)
	{
		_num = round(random(length));
		
		if (is_undefined(ds_map_find_value(map, _num)))
		{
			ds_map_add(map, _num, "moot");
		
			TEXT = ini_read_string(sect, _num, "none");
		
			if (TEXT != "none")
			{
				if (global.type == "drawn")
				{
					MultiTextGenerator();
					break;
				}
				else show_message(TEXT);
			}
		}
		else
		{
			i --;
			show_debug_message("missed");
		}
	}
	ds_map_destroy(map);
	show_debug_message("Done!");
}
#endregion

#region SECTIONS
else if (type == "sections") room_goto(rSections);
#endregion

#region RANDOM SINGLE
else if (type == "random single")
{
	do TEXT = ini_read_string(sect, round(random(length)), "none");
	until (TEXT != "none");
	
	if (global.type == "drawn")
	{
		NewTextBox(TEXT, 1);
	}
	else show_message(TEXT);
}
#endregion

#region SWITCH
else if (type = "switch")
{
	var tempType = global.type;
	if (tempType == "drawn")
	{
		ini_key_delete("Controls", "Type");
		ini_write_string("Controls", "Type", "popup");
		global.type = "popup";
	}
	else if (tempType == "popup")
	{
		ini_key_delete("Controls", "Type");
		ini_write_string("Controls", "Type", "drawn");
		global.type = "drawn";
	}
}
#endregion

#region ADD
else if (type == "add")
{
	AddScript(get_string("How many letters do you want to add?", 0));
}
#endregion

#region RESET
else if (type = "reset")
{
	if (show_question("Are you sure you want to reset EVERYTHING?"))
	{
		file_delete(file);
		game_restart();
	}
}
#endregion

//Catch Statement
else show_message("Error!");

ini_close();