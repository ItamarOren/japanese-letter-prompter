///@description
draw_self();
str = string_hash_to_newline(string_replace(type, " ", "#"));

if (type == "reset")
{
	sprite_index = sReset;
	c = c_white;
}
else c = c_red;

if (type == "switch")
{
	var _text;
	if (global.type == "drawn") _text = "Pop Up";
	else if (global.type == "popup") _text = "Drawn";
	else show_debug_message("SOMETHING WENT WRONG");
	
	draw_text_color(bbox_left + 5, y - 20, _text, c, c, c, c, 1);
}
else draw_text_color(bbox_left + 5, y - 20, str, c, c, c, c, 1);