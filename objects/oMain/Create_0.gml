///@description
#macro file "letters.ini"
#macro sect "Letters"
globalvar TEXT;
globalvar MODMODE;
MODMODE = false;


if (!file_exists(file))
{
	ini_open(file);
	
	if (show_question("Do you want to add sections to your letters?")) sectnum = real(get_string("How many sections (use numbers)?", ""));
	else sectnum = 1;
	
	if (sectnum > 1)
	{
		var sectName, _lll, _t, _lt, first;
		_lt = 0;
		first = true
		_lettersTotal = 0;
		
		for (var i = 0; i < sectnum; i ++)
		{
			sectName = get_string("What is section " + string(i + 1) +"'s name? (k, n, etc)", "");
			_lll = real(get_string("How many 'words' in this section?", ""));
			global.sectName[i] = sectName;

			if (first = true)
			{
				first = false;
			}
			
			ini_write_string(sectName, "Total", _lll);
			
			for (var p = 0; p < _lll; p ++)
			{
				_t = get_string("Input ENGLISH letter #" + string(p + 1), "");
				ini_write_string(sectName, p, _t);
				ini_write_string(sect, _lettersTotal + p, _t);
			}
			_lettersTotal += _lll;
		}
		ini_write_real(sect, "Total", _lettersTotal);
		ini_close();
		
		ini_open(file);
		
		global.VidTextArgs[0] = 1;
		for (var i = 0; i < ini_read_real(sect, "Total", 0); i ++)
		{
			global.VidTextArgs[i + 1] = ini_read_string(sect, i, -1);
		}
	}
	else
	{
		var howMany = real(get_string("How many letters do you want to add?", 0));
		ini_write_real(sect, "Total", howMany);
		
		if (howMany = 0 || howMany == "0")
		{
			ini_close();
			if (file_exists(file)) file_delete(file);
			game_restart();
		}
		else
		{
			if (show_question("You selected to enter " + string(howMany) + " characters. Is this correct?")) 
			{
				ini_open(file);
				global.VidTextArgs[0] = 1;
				for (var i = 0; i < howMany; i ++)
				{
					var _text = get_string("Input ENGLISH letter #" + string(i + 1), "");
					ini_write_string(sect, i, _text);
					global.VidTextArgs[i + 1] = _text;
				}
			}
		}
	}
	
	ini_write_string("Controls", "Type", "drawn");
	global.type = "drawn";
	ini_close();
}
else
{
	ini_open(file);
	
	var _lSect = ini_read_real("Sections", "Total", 0);
	for (var sss = 0; sss < _lSect; sss ++)
	{
		global.sectName[sss] = ini_read_string("Sections", sss, "none");
	}
	
	global.type = ini_read_string("Controls", "Type", "drawn");
	
	length = ini_read_real(sect, "Total", 0);
	global.VidTextArgs[0] = 1;
	
	for (var i = 0; i < length; i ++)
	{
		var _text = ini_read_string(sect, i, undefined);
		if (_text != undefined)
		{
			global.VidTextArgs[i + 1] = _text;
		}
	}
	
	ini_close();
}
