///@description
ini_open(file);
var num = real(ini_read_string("Letters", "Total", -1));
var RES_H, RES_W, _length, h;
RES_H = display_get_gui_height();
RES_W = display_get_gui_width();
_length = array_length(global.sectName);

for (var i = 0; i < _length; i ++)
{
	if (i < 7) h = RES_H/2;
	else h = RES_H/4;
	
	with (instance_create_layer(100 + (i * 100), h, layer, SwitchButton))
	{
		section = global.sectName[i];
	}
}