NineSliceBoxStretched(sTextBoxBg, x1, y1, x2, y2, background, portrait);
draw_set_font(fText)
//draw_set_halign(fa_center);
//draw_set_valign(fa_top);
draw_set_color(c_black);

var _print = string_copy(message, 1, textProgress)


//Add responses
if (responses[0] != -1) && (textProgress >= string_length(message))
{
	for (var i = 0; i < array_length(responses); i++)
	{
		_print += "\n";
		if (i == responseSelected) _print += "> ";
		_print += responses[i];
		if (i == responseSelected) _print += " <";
	}
}

//Draw Text
draw_set_color(c_white);
draw_text_transformed((x1+x2) /2, y1+12, string_hash_to_newline(_print), 4, 4, 0);

//Shadow
draw_text_transformed((x1+x2) /2, y1+14, string_hash_to_newline(_print), 4, 4, 0);
