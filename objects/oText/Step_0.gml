lerpProgress += (1 - lerpProgress) /50;
textProgress += global.textSpeed;

x1 = lerp(x1, x1Target, lerpProgress);
x2 = lerp(x2, x2Target, lerpProgress);


//Cycle through responses
keyUp = keyboard_check_pressed(vk_up) || keyboard_check_pressed(ord("W"));
keyDown = keyboard_check_pressed(vk_down) || keyboard_check_pressed(ord("S"));
responseSelected += (keyDown - keyUp);
var _max = array_length(responses)-1;
var _min = 0;
if (responseSelected > _max) responseSelected = _min;
if (responseSelected < _min) responseSelected = _max;

//End Message
if (keyboard_check_pressed(vk_space))
{
	var _messageLength = string_length(message)
	if (textProgress >= _messageLength)
	{
		
		if (responses[0] != -1)
		{
			breakpoint = 0;
			with (originInstance)
			{
				//This Refers to the NPC that is talking IF IT IS AN OBJECT
				//DialogueResponses(other.responseScripts[other.responseSelected]);
				//MultiResponseScript(other.responseScripts[other.responseSelected]);
			}
		}
		if (array_length(responses) < 2)
		{
			if (textScript != -1)
			{
			DialogueScriptManager(textScript);
			}
		}
		else
		{
			if (textScript[responseSelected] != -1)
			{
				DialogueScriptManager(textScript[responseSelected]);
			} else show_debug_message("NO SCRIPT");
		}
		
		instance_destroy();
		if (instance_exists(oTextQueued))
		{
			with (oTextQueued) ticket--;	
		}
		else
		{
			game_restart();
		}
	}
	else
	{
		if (textProgress > 2)
		{
			textProgress = _messageLength;
		}
	}
}