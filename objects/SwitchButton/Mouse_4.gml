///@description
if (global.type == "drawn")
{
	array_delete(global.VidTextArgs, 0, array_length(global.VidTextArgs));
	global.VidTextArgs[0] = 1;
	
	for (var i = 0; i < real(ini_read_string(section, "Total", 0)); i ++)
	{
		global.VidTextArgs[i + 1] = ini_read_string(section, i, "none");
	}
	MultiTextGenerator();
}
else
{
	for (var i = 0; i < real(ini_read_string(section, "Total", 0)); i ++)
	{
		show_message(ini_read_string(section, i, "none"));
	}
}