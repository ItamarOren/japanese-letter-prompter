/// @arg Message
/// @arg Background
/// @arg [Responses]
function NewTextBox() {

	var _obj;
	if (instance_exists(oText)) _obj = oTextQueued; else _obj = oText;
	with (instance_create_layer(0, 0, "Instances", _obj))
	{
		//message = argument[0];
		text = argument[0];
		
		#region Scripts
		//message*script*
		
		for (i = 0; i < argument_count; i ++)
		{
			if (string_pos("*", text) != 0)
			{
				show_debug_message(i);
				//Script
				var start, tempScript, fin;
				start = string_pos("*", text);
				tempScript = string_delete(text, 1, start);
				fin = string_pos("*", tempScript);
				tempScript = string_copy(tempScript, 1, fin - 1);

				//Text
				var commandStart, commandFin, tempText, textAfterCommand;
				commandStart = string_pos("*", text);
				text = string_delete(text, commandStart, string_length(tempScript) + 2);

				textScript = tempScript;
			} else textScript = -1;
		}
		
		#endregion
		
		var _textPosition = string_pos(":", text);
		text = string_delete(text, 1, _textPosition);
		message = text;
		
		#region Setting the Portrait

		//Portrait
		//trim markers from getPortrait
		//if (argument_count > 1) spot = 1; MAYBE LATER
		spot = 0;
		getPortrait = argument[spot];
		var _PortraitMarker = string_pos(":", getPortrait);
		if (_PortraitMarker = 0) portrait = sNOTHING;
		else
		{
		var _lll = string_length(argument[spot]);
		getPortrait = string_delete(getPortrait, _PortraitMarker, _lll);

		if (string_pos("CHOICE", getPortrait) == 0)
		{
			tPortrait = sNOTHING;
		}

		else tPortrait = asset_get_index("s" + getPortrait);
		//show_message("s" + getPortrait + " says" + message);
		}
	#endregion

	
		if (instance_exists(other)) originInstance = other.id; else originInstance = noone;
		if (argument_count > 1) background = argument[1]; else background = 1;
		
		//Argument[2] is an array
		if (argument_count > 2)
		{
			portrait = sNOTHING;
			//trim markers from responses
			responses = argument[2];
			numOfResponse = array_length(responses);
			
			for (var i = 0; i < numOfResponse; i++)
			{
				show_debug_message(responses[i]);
				var _markerPosition = string_pos("-", responses[i]);

				#region Scripts
				text = responses[i];
				if (string_pos("*", text) != 0)
				{
					show_debug_message(i);
					//Script
					var start, tempScript, fin;
					start = string_pos("*", text);
					tempScript = string_delete(text, 1, start);
					fin = string_pos("*", tempScript);
					tempScript = string_copy(tempScript, 1, fin - 1);

					//Text
					var commandStart, commandFin, tempText, textAfterCommand;
					commandStart = string_pos("*", text);
					responses[i] = string_delete(text, commandStart, string_length(tempScript) + 2);

					textScript[i] = tempScript;
				} else textScript[i] = -1;
				
				#endregion
				//responses[i] = string_delete(responses[i], 1, _markerPosition);
				//responses[i] = string_delete(responses[i], string_pos("*", responses[i]), string_length(responses[i]));
				breakpoint = 10;
			}
		}
		else
		{
			responses = [-1];
			responseScripts = [-1];
		}
	}
}