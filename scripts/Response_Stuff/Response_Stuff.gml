function MultiResponseScript(argument0) {
	ini_open("file.ini")
	for (i = NUM; i > 0 ; i --)
	{
		switch(argument0)
		{
		
			case i: script_execute(asset_get_index(gResponseScript[i]), room);
			break;
		} 
	}
	ini_close(); 


}

function TextChoiceScript(title, argArray) {

	var _msg = title;
	var _args = argArray;

	switch (array_length(_args)) {

		case 1: return NewTextBox(_msg, 1, [_args[0]]); 
		
		case 2: return NewTextBox(_msg, 1, [_args[0], _args[1]]); 
		
		case 3: return NewTextBox(_msg, 1, [_args[0], _args[1], _args[2]]); 
		 
		case 4: return NewTextBox(_msg, 1, [_args[0], _args[1], _args[2], _args[3]]); 
		
		case 5: return NewTextBox(_msg, 1, [_args[0], _args[1], _args[2], _args[3], _args[4]]);
		
		case 6: return NewTextBox(_msg, 1, [_args[0], _args[1], _args[2], _args[3], _args[4], _args[5]]); 
		
		case 7: return NewTextBox(_msg, 1, [_args[0], _args[1], _args[2], _args[3], _args[4], _args[5], _args[6]]); 
		 
		case 8: return NewTextBox(_msg, 1, [_args[0], _args[1], _args[2], _args[3], _args[4], _args[5], _args[6], _args[7]]); 

		default: show_error("script_execute_array: argument count not supported! \nPlease cut your choice entries down to 5 or less please! \n ", false);
	}
}