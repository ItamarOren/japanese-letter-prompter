///@arg Output_for_MultiTextKeeper
function MultiTextGenerator() {

	var background, length, i;

	background = global.VidTextArgs[0];
	length = array_length(global.VidTextArgs); //Maybe

	rep = true;

	for (i = 1; i < length; i ++)
	{
		var str = global.VidTextArgs[i];

		#region CHOICES
		
		if (string_pos("CHOICE", str) != 0)
		{
			var marker, choice, cNum, mark2, _lll, _forLength;
		
			//CHOICE: 1-choice1; 2-choice2;
			_forLength = string_count("-", str);
		
			for (var i = 0; i < _forLength; i ++)
			{
				marker = string_pos("-", str);
				mark2 = string_pos(";", str);
				_lll = string_length(str);
			
				cNum[i] = string_copy(str, marker - 1, 1); //Find the number before the choice
				choice[i] = string_copy(str, marker + 1, mark2 - marker - 1);
				str = string_delete(str, 1, mark2);
			}
			TextChoiceScript("CHOICE", choice);
		}
	
		#endregion
		
		else
		{
			NewTextBox(str, background);
		}
	}


}
