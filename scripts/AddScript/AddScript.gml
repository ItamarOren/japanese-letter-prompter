//Put the general Script's Group name here
function AddScript(howMany){
	var length = ini_read_real("Numbers", "Total", 0) + 1;
	var newLength = length + real(howMany);
	
	ini_open(file);
	if (show_question("Do you want to add something to a specific section? (Not make a new one)"))
	{
		var _tempSect = get_string("What is the section's name?", "");
		var _howMany = real(get_string("How many do you want to add?", ""));
		var _newTotal, _mainTotalNew;
		var _mainTotalOld = ini_read_real(sect, "Total", 0);
		
		for (var p = 0; p < _howMany; p ++)
		{
			//Specific
			var _newLetter = get_string("Input ENGLISH letter #" + string(p + 1), "");
			var _total = ini_read_real(_tempSect, "Total", 0);
			_newTotal = _total + p;
			ini_write_string(_tempSect, _newTotal, _newLetter);
			
			//Main
			_mainTotalNew = _mainTotalOld + p;
			ini_write_string(sect, _mainTotalNew, _newLetter);
		}
		ini_write_real(sect, "Total", _mainTotalNew);
		ini_write_real(_tempSect, "Total", _newTotal);
	}
	else
	{
		var sectName, _lll, _t, _lt, first;
		_lt = 0;
		var _newTotal, _mainTotalNew;
		var _mainTotalOld = ini_read_real(sect, "Total", 0);
		_lll = real(get_string("How many 'words' in this section?", ""));
		
		sectName = get_string("What the section 's name? (k, n, etc)", "");
		global.sectName[array_length(global.sectName) + 1] = sectName;
		
		for (var p = 0; p < _lll; p ++)
		{
			_t = get_string("Input ENGLISH letter #" + string(p + 1), "");
			ini_write_string(sect, _mainTotalOld + p, _t);
		}
		_mainTotalNew = _mainTotalOld + _lll;
		ini_write_real(sect, "Total", _mainTotalNew);
	}
	ini_close();
	
	ini_open(file);
	for (var i = 0; i < ini_read_real(sect, "Total", 0); i ++)
	{
		//Maybe i + 1?
		global.VidTextArgs[i] = ini_read_string(sect, i, -1);
	}
	
	for (var i = length; i < newLength; i ++)
	{
		ini_write_string(sect, i, get_string("Input ENGLISH letter #" + string(i), ""));
	}
	
	ini_write_real("Numbers", "Total", newLength);
	ini_close();
}