///@arg text
///@arg delete_before_or_after
///@arg marker
///@arg "input,type,input,type,etc,"
function CheckerScript() {

	var _length, _text, i, _markerPosition, _newText, _newType, _last;

	#region Achievement Section
	if (argument_count <= 1)
	{
		_text = argument[0];
		for (i = 0 ; i < string_count(",", _text); i ++)
		{
			_text = argument[0];
			_length = string_count(",", _text) + 1;

			for (i = 0; i < _length; i += 2)
			{
				//Text
				_markerPosition = string_pos(",", _text);
				_newText = string_copy(_text, 1, _markerPosition-1);
				_text = string_delete(_text, 1, _markerPosition);

				//Key
				_markerPosition = string_pos(",", _text);
				_newType = string_copy(_text, 1, _markerPosition-1);
				_text = string_delete(_text, 1, _markerPosition);

				global.ActivateAchievementArgs[i] = _newText;
				global.ActivateAchievementArgs[i + 1] = _newType;

				//show_message(_newType + ": type");
				//show_message(_newText + ": text");
			}
		}
		ActivateAchievement(global.ActivateAchievementArgs);
	}
	#endregion
	else
	{
		_text = argument[0];
		_length = string_length(_text);
		_markerPosition = string_pos(argument[2], _text);

		if (argument[1] = "a")
		{
			_text = string_delete(_text, _markerPosition, _length);
		}
		else
		{
			_text = string_delete(_text, 1, _markerPosition);
		}
		return _text;
	}
}