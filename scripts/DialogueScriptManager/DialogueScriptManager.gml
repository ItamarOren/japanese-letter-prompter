function DialogueScriptManager(arg) {

	if (string_pos("room_goto", arg)) room_goto_ext(arg);
	else if (string_pos("show_message", arg)) show_message(arg);
	else if (string_pos("ActivateAchievement", arg)) AchievementAdderText(arg);
	else if (string_pos("CustomKeyAdd", arg)) CustomKeyAdd_ext(arg);
	else if (string_pos("room_restart()", arg)) room_restart();
	
	else
	{
		instance_destroy(oText);
		if (instance_exists(oTextQueued)) oTextQueued.ticket ++;
	}
}
